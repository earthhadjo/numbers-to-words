// defining the variable that contain the words I want to replace with numbers
const zerotonine = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
const teensName = ["", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
const tenName = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
const hundredName = ["hundred", "thousand"]

//takes in number to display to screen
function display(wordArray) {
    let output = document.getElementById('output');
    let div = document.createElement('div');
    let text = document.createTextNode(wordArray)
    div.appendChild(text)
    output.appendChild(div);

}

// created a function that converts numbers to words
function numberstowords() {
    // displays zero to 9 array 
    for (i = 0; i <= 9; i++) {
        if (i <= 9) {
            display(zerotonine[i])
        }

    }
    // displays 10 thru 19 teens
    for (i = 10; i <= 20; i++) {
        if (i <= 20) {
            display(teensName[i - 10])
        }
    }
    // displays 20 thru 99 
    for (let num of tenName) {
        display(num)
        for (i = 1; i <= 9; i++) {
            display(num + "-" + zerotonine[i])
        }
    }

}
function numtoHundred() {
    // displays 100 thru 999 
    for (let j = 1; j <= 9; j++) {
        display(zerotonine[j] + " " + "hundred")
        for (i = 1; i <= 9; i++) {
                display(zerotonine[j] + " " + "hundred " + zerotonine[i])

        }
        for (i = 11; i <= 20; i++) {
            display(zerotonine[j] + " " + "hundred " + teensName[i - 10])
        }

        for (let num of tenName) {
            display(zerotonine[j] + " " + "hundred " + num)
            for (i = 1; i <= 9; i++) {
                display(zerotonine[j] + " " + "hundred " + num + "-" + zerotonine[i])
            }
        }
    }

    display("one thousand")
}
numberstowords()
numtoHundred()






